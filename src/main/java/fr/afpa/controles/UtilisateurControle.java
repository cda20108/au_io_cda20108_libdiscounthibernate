package fr.afpa.controles;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Utilisateur;
import fr.afpa.metiers.UtilisateurMetier;

public class UtilisateurControle {

	private UtilisateurMetier metier;

	public UtilisateurControle() {
		metier = new UtilisateurMetier();
	}

	/**
	 * Pour verifier le format de NOM
	 * 
	 * @param nom
	 * @return
	 */
	public boolean checkNom(String nom) {
		return nom != null && nom.length() <= 100 && nom.matches("[\\pL\\s]+?");
	}

	/**
	 * Pour verifier le format de PRENOM
	 * 
	 * @param prenom
	 * @return
	 */
	public boolean checkPrenom(String prenom) {
		return prenom != null && prenom.length() <= 100 && prenom.matches("[\\pL\\s]+?");
	}

	/**
	 * Pour verifier le format de NOM_LIBRAIRIE
	 * 
	 * @param nomLibrairie
	 * @return
	 */
	public boolean checkNomLibrairie(String nomLibrairie) {
		return nomLibrairie != null && nomLibrairie.length() <= 150 && nomLibrairie.matches("[\\pL\\s]+?");
	}

	/**
	 * Pour verifier le format de EMAIL
	 * 
	 * @param email
	 * @return
	 */
	public boolean checkEmail(String email) {
		return email != null && email.length() <= 150
				&& email.matches("^[0-9a-zA-Z-_\\.]{5,}@[0-9a-zA-Z-\\.]+\\.[a-zA-Z]{2,6}?");
	}

	/**
	 * Pour verifier le format de numero de Tele
	 * 
	 * @param numTele
	 * @return
	 */
	public boolean checkNumTel(String numTele) {
		return numTele != null && numTele.length() == 10 && numTele.matches("0[1-9]{1}[0-9]{8}");
	}

	/**
	 * Pour recupper les info user pour creer un nouveau utilisateur
	 * 
	 * @param nom
	 * @param prenom
	 * @param nomLibrairie
	 * @param numTel
	 * @param email
	 * @param adresse
	 * @return
	 */
	public Utilisateur addUtilisateur(String nom, String prenom, String nomLibrairie, String numTel, String email,
			Adresse adresse) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		utilisateur.setNomLibrairie(nomLibrairie);
		utilisateur.setEMail(email);
		utilisateur.setNumTel(numTel);
		utilisateur.setAdresse(adresse);
		return metier.addUtilisateur(utilisateur);	
	}



	/**
	 * POUR mettre a jour un utilisateur
	 * 
	 * @param user
	 */
	public void updateUtilisateur(Utilisateur user) {
		metier.updateUtilisateur(user);

	}


}

package fr.afpa.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQuery(name = "findByLoginAndPassword", query = "from Auth where login = :login and password = :password")
@NamedQuery(name = "findByUser", query = "from Auth where utilisateur = :user")
public class Auth implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4630936431931029247L;

	private String login;

	private String password;

	private boolean activate;

	@OneToOne
	@Id
	@JoinColumn(name = "id_utilisateur")
	private Utilisateur utilisateur;

	/**
	 * 
	 */
	public String toString() {
		return "LOGIN : " + login + " , PASSWORD : " + password;
	}

	public Auth(String login, String password, Utilisateur utilisateur) {
		this.login = login;
		this.password = password;
		this.utilisateur = utilisateur;
		this.activate = true;
	}
}

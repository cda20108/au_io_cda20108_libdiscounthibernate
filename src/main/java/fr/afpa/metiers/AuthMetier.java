package fr.afpa.metiers;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.AuthDAO;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthMetier {

	
	public Auth getAuth(Utilisateur user) {
		return new AuthDAO().getAuth(user);
	}
	
	public Auth getAuth(String login,String password) {
		return new AuthDAO().getAuth(login,password);
	}

	public void addAuth(String login, String password, Utilisateur utilisateur) {
		new AuthDAO().createAuth(new Auth(login, password, utilisateur));
		
	}

	public void updateAuth(Auth auth) {
		new AuthDAO().updateAuth(auth);
	}

//	/**
//	 * Pour verifier si le login et le password sont correctes
//	 * 
//	 * @param auth
//	 * @return
//	 */
//	public int checkAuth(Auth auth) {
//		return new AuthDAO().checkAuth(auth);
//	}
	
	

}

package fr.afpa.vues;

import java.util.Scanner;

import fr.afpa.controles.UtilisateurControle;
import lombok.AllArgsConstructor;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
@AllArgsConstructor
public class UserFields {

	private Scanner in;
	private UtilisateurControle uc;

	/**
	 * Pour recupper le saisie clavier pour le NOM
	 * 
	 * @param msg
	 * @return
	 */
	public String nomField(String msg) {
		System.out.print(msg);
		String nom = in.nextLine();
		while (!uc.checkNom(nom)) {
			System.out.print("Mauvaise saisie , " + msg);
			nom = in.nextLine();
		}
		return nom;
	}

	/**
	 * Pour recupper le saisie clavier pour le PRENOM
	 * 
	 * @param msg
	 * @return
	 */
	public String prenomField(String msg) {
		System.out.print(msg);
		String prenom = in.nextLine();
		while (!uc.checkPrenom(prenom)) {
			System.out.print("Mauvaise saisie , " + msg);
			prenom = in.nextLine();
		}
		return prenom;
	}

	/**
	 * Pour recupper le saisie clavier pour le NOM_LIBRAIRIE
	 * 
	 * @param msg
	 * @return
	 */
	public String nomLibrairieField(String msg) {
		System.out.print(msg);
		String nomLibrairie = in.nextLine();
		while (!uc.checkNomLibrairie(nomLibrairie)) {
			System.out.print("Mauvaise saisie , " + msg);
			nomLibrairie = in.nextLine();
		}
		return nomLibrairie;
	}

	/**
	 * Pour recupper le saisie clavier pour le NUMERO DE TELEPHONE
	 * 
	 * @param msg
	 * @return
	 */
	public String numTelField(String msg) {
		System.out.print(msg);
		String numTel = in.nextLine();
		while (!uc.checkNumTel(numTel)) {
			System.out.print("Mauvaise saisie , " + msg);
			numTel = in.nextLine();
		}
		return numTel;
	}

	/**
	 * Pour recupper le saisie clavier pour l'EMAIL
	 * 
	 * @param msg
	 * @return
	 */
	public String emailField(String msg) {
		System.out.print(msg);
		String email = in.nextLine();
		while (!uc.checkEmail(email)) {
			System.out.print("Mauvaise saisie , " + msg);
			email = in.nextLine();
		}
		return email;
	}

	/**
	 * Pour questionner l'utilisateur s'il vaux fair e une modification
	 * 
	 * @param msg
	 * @return
	 */
	public boolean questionField(String msg) {
		System.out.print("Voulez vous changer " + msg + " (1 pour valider autre si non) >>");
		String choix = in.nextLine();
		return "1".equalsIgnoreCase(choix.replaceAll(" ", ""));
	}

}

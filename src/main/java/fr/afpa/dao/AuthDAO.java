package fr.afpa.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Auth;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
public class AuthDAO {

	public void createAuth(Auth auth) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.persist(auth);
		tx.commit();
		session.close();
	}

	public Auth getAuth(String login, String password) {

		Session session = HibernateUtils.getSession();

		Query req = session.getNamedQuery("findByLoginAndPassword");

		req.setParameter("login", login);
		req.setParameter("password", password);

		Auth auth = req.getResultList().isEmpty() ? null : (Auth) req.getResultList().get(0);

		session.close();

		return auth;
	}

	public Auth getAuth(Utilisateur user) {

		Session session = HibernateUtils.getSession();
		Query req = session.getNamedQuery("findByUser");
		req.setParameter("user", user);
		Auth auth = req.getResultList().isEmpty() ? null : (Auth) req.getResultList().get(0);
		session.close();
		return auth;
	}

	/**
	 * Pour mettre � jour le mot de passe d'un utilisateur en fonction de son id
	 * 
	 * @param id_user
	 * @param new_password
	 */
	public void updateAuth(Auth auth) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.update(auth);

		tx.commit();
		session.close();

	}

}

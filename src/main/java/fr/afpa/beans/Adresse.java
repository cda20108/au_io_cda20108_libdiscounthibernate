package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author CDA
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_adresse")
	private int idAdresse;

	@NonNull
	private String numero;

	@NonNull
	private String libelle;
	@NonNull
	private String cp;

	@NonNull
	private String ville;

	/**
	 *
	 */
	public String toString() {
		return "       >> adresse : " + numero + " " + libelle + " ," + cp + " " + ville;
	}
	
}

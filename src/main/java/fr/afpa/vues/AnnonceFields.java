package fr.afpa.vues;

import java.util.Scanner;

import fr.afpa.controles.AnnonceControle;
import lombok.AllArgsConstructor;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
@AllArgsConstructor
public class AnnonceFields {
	
	private Scanner in;
	private AnnonceControle anc;
	
	/**
	 * Pour recupperer le saisie clavier pour le TITRE et MAISON_EDITION
	 * 
	 * @param msg
	 * @return
	 */
	public String titre_MaisonEdition_Field(String msg) {
		System.out.print(msg);
		String titre_MeEd = in.nextLine();
		while (!anc.checkTitre_MaisonEdition(titre_MeEd)) {
			System.out.print("Mauvaise saisie , " + msg);
			titre_MeEd = in.nextLine();
		}
		return titre_MeEd;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour le ISBN
	 * 
	 * @param msg
	 * @return
	 */
	public String isbnField(String msg) {
		System.out.print(msg);
		String isbn = in.nextLine();
		while (!anc.checkISBN(isbn)) {
			System.out.print("Mauvaise saisie , " + msg);
			isbn = in.nextLine();
		}
		return isbn;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour la DATE_EDITION
	 * 
	 * @param msg
	 * @return
	 */
	public String dateEditionField(String msg) {
		System.out.print(msg);
		String date = in.nextLine();
		while (!anc.checkDateEdition(date)) {
			System.out.print("Mauvaise saisie , " + msg);
			date = in.nextLine();
		}
		return date;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour la REMISE
	 * 
	 * @param msg
	 * @return
	 */
	public String remiseField(String msg) {
		System.out.print(msg);
		String remise = in.nextLine();
		while (!anc.checkRemise(remise)) {
			System.out.print("Mauvaise saisie , " + msg);
			remise = in.nextLine();
		}
		return remise;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour la QUANTITE
	 * 
	 * @param msg
	 * @return
	 */
	public String quantiteField(String msg) {
		System.out.print(msg);
		String qte = in.nextLine();
		while (!anc.checkQuantite(qte)) {
			System.out.print("Mauvaise saisie , " + msg);
			qte = in.nextLine();
		}
		return qte;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour le PRIX_UNITAIRE
	 * 
	 * @param msg
	 * @return
	 */
	public String prixUnitaireField(String msg) {
		System.out.print(msg);
		String prix = in.nextLine();
		while (!anc.checkPrixUnitaire(prix)) {
			System.out.print("Mauvaise saisie , " + msg);
			prix = in.nextLine();
		}
		return prix;
	}
	
	/**
	 * Pour recupperer le saisie clavier pour l' ID_ANNONCE
	 * 
	 * @param msg
	 * @return
	 */
	public String idAnnonceField(String msg) {
		System.out.print(msg);
		String id = in.nextLine();
		while (!anc.checkIDAnnonce(id)) {
			System.out.print("Mauvaise saisie , " + msg);
			id = in.nextLine();
		}
		return id;
	}
	
	/**
	 * Pour questionner l'utilisateur s'il vaux fair e une modification
	 * 
	 * @param msg
	 * @return
	 */
	public boolean questionField(String msg) {
		System.out.print("Voulez vous changer " + msg + " (1 pour valider autre si non) >>");
		String choix = in.nextLine();
		return "1".equalsIgnoreCase(choix.replaceAll(" ", ""));
	}
	
}

package fr.afpa.vues;

import java.util.Scanner;

import fr.afpa.controles.AdresseControle;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AdresseFields {

	private Scanner in;
	private AdresseControle ac;

	/**
	 * Pour recupperer le saisie clavier pour le NUMERO
	 * 
	 * @param msg
	 * @return
	 */
	public String numeroField(String msg) {
		System.out.print(msg);
		String numero = in.nextLine();
		while (!ac.checkNumero(numero)) {
			System.out.print("Mauvaise saisie , " + msg);
			numero = in.nextLine();
		}
		return numero;
	}

	/**
	 * Pour recupperer le saisie clavier pour le LIBELLE
	 * 
	 * @param msg
	 * @return
	 */
	public String libelleField(String msg) {
		System.out.print(msg);
		String libelle = in.nextLine();
		while (!ac.checkLibelle(libelle)) {
			System.out.print("Mauvaise saisie , " + msg);
			libelle = in.nextLine();
		}
		return libelle;
	}

	/**
	 * Pour recupperer le saisie clavier pour le CODE_POSTALE
	 * 
	 * @param msg
	 * @return
	 */
	public String cpField(String msg) {
		System.out.print(msg);
		String cp = in.nextLine();
		while (!ac.checkCP(cp)) {
			System.out.print("Mauvaise saisie , " + msg);
			cp = in.nextLine();
		}
		return cp;
	}

	/**
	 * Pour recupperer le saisie clavier pour la VILLE
	 * 
	 * @param msg
	 * @return
	 */
	public String villeField(String msg) {
		System.out.print(msg);
		String ville = in.nextLine();
		while (!ac.checkVille(ville)) {
			System.out.print("Mauvaise saisie , " + msg);
			ville = in.nextLine();
		}
		return ville;
	}

	/**
	 * Pour questionner l'utilisateur s'il vaux fair e une modification
	 * 
	 * @param msg
	 * @return
	 */
	public boolean questionField(String msg) {
		System.out.print("Voulez vous changer " + msg + " (1 pour valider autre si non) >>");
		String choix = in.nextLine();
		return "1".equalsIgnoreCase(choix.replaceAll(" ", ""));
	}

}

package fr.afpa.vues;

import java.util.Scanner;

import fr.afpa.controles.AuthControle;
import lombok.AllArgsConstructor;

/**
 * 
 * @author AU_LIBDISCOUNT
 *
 */
@AllArgsConstructor
public class AuthFields {

	private Scanner in;
	private AuthControle ac;

	/**
	 * Pour recupperer le saisie clavier pour le LOGIN
	 * 
	 * @param msg
	 * @return
	 */
	public String loginField(String msg) {
		System.out.print(msg);
		String login = in.nextLine();
		while (!ac.checkLogin(login)) {
			System.out.print("Mauvaise saisie , " + msg);
			login = in.nextLine();
		}
		return login;
	}

	/**
	 * Pour recupperer le saisie clavier pour le PASSWORD
	 * 
	 * @param msg
	 * @return
	 */
	public String passwordField(String msg) {
		System.out.print(msg);
		String password = in.nextLine();
		while (!ac.checkPassword(password)) {
			System.out.print("Mauvaise saisie , " + msg);
			password = in.nextLine();
		}
		return password;
	}

}
